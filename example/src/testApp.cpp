#include "testApp.h"


void 
testApp::setup()
{
  ofSetVerticalSync(true);

  // you can test this quickly with "php -S localhost:8080"
  action_url = "http://localhost:8080/upload.php";

  ofBackground(255,255,255);
  ofSetColor(0,0,0);
}


void 
testApp::update()
{

}


void 
testApp::draw()
{
  ofDrawBitmapString(request_str,20,20);
  ofDrawBitmapString(response_str,20,60);
}


void 
testApp::keyPressed  (int key)
{
  ofxREST::FormRequest form (action_url);
  form.add_field ("number", ofToString(counter));
  form.add_file ("file","ofw-logo.gif");

  request_str = "message sent: " + ofToString(counter);
  ofxREST::Response response = agent.POST (form);
  response_str = ofToString (response.status) + ": " + (string) response.body;
  counter++;
}


void 
testApp::mouseMoved(int x, int y )
{

}


void 
testApp::mouseDragged(int x, int y, int button)
{

}

void 
testApp::mousePressed(int x, int y, int button)
{

}


void 
testApp::mouseReleased(int x, int y, int button)
{

}


void 
testApp::windowResized(int w, int h)
{

}
