#include "ofxREST.h"

#include <curl/curl.h>
#include <http_parser.h>

// static helper functions
static string generate_request_url (ofxREST::FormRequest & form);
static string get_url_filename (const std::string &url);


namespace ofxREST {

  struct Agent::pimpl {

    CURL *curl;
    http_parser_settings parser_settings;
    ofBuffer response_buffer;
    bool verbose;
    int timeout;

    pimpl(){};
    ~pimpl(){};
   
    Response 
    parse_response_buffer()
    {
      // for now just return a buffer with the raw response
      Response response;
    
      response.status = 200;
      response.reason = "OK";
      response.body = response_buffer;

      //http_parser parser;
      //http_parser_init (parser, HTTP_RESPONSE);
      
      return response;
    }
    
    static size_t 
    fill_response_buffer (char *ptr, size_t size, 
			  size_t nmemb, void *userdata)
    {  
      pimpl *mp = (pimpl *) userdata;
    
      size_t write_size = size * nmemb;
      mp->response_buffer.append (ptr, write_size);;
    
      return write_size;
    }
  };


  Agent::Agent():
    mp (new pimpl)
  {
    mp->verbose = true;
    mp->timeout = 0;

    curl_global_init(CURL_GLOBAL_ALL);
    mp->curl = curl_easy_init();
    curl_easy_setopt (mp->curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
    curl_easy_setopt (mp->curl, CURLOPT_FOLLOWLOCATION, 1); 
    
    // TODO: parser settings

    if (! mp->curl) {
      if (mp->verbose) {
	ofLogError() << "Agent: curl library not initialized properly";
      }
    }
  }

  Agent::~Agent()
  {
    curl_easy_cleanup (mp->curl);
    delete mp;
  }

  Response 
  Agent::GET (const std::string &url)
  {    
    return ofxREST::Response();
  }

  Response 
  Agent::GET (const std::string &url, const std::string &save_path)
  {
    return ofxREST::Response();
  }

  Response
  Agent::GET (const FormRequest &form)
  {
    return ofxREST::Response();
  }

  Response 
  Agent::POST (const FormRequest &form)
  {
    // prepare form
    struct curl_httppost *p_form_start=NULL;
    struct curl_httppost *p_form_end=NULL;

    const FormRequest::value_map &values = form.get_values();
    FormRequest::value_map::const_iterator vit = values.begin();
    
    while (vit != values.end()) {
      curl_formadd (&p_form_start, &p_form_end,
		    CURLFORM_COPYNAME, vit->first.c_str(),
		    CURLFORM_COPYCONTENTS, vit->second.c_str(),
		    CURLFORM_END);
      vit++;
    }

    const FormRequest::value_map &files = form.get_files();
    FormRequest::value_map::const_iterator fit = files.begin();
    
    while (fit != files.end()) {
      curl_formadd (&p_form_start, &p_form_end,
		    CURLFORM_COPYNAME, fit->first.c_str(),
		    CURLFORM_FILE, fit->second.c_str(),
		    CURLFORM_END);
      fit++;
    }
    
    curl_easy_setopt (mp->curl, CURLOPT_URL, form.get_action().c_str());
    curl_easy_setopt (mp->curl, CURLOPT_HTTPPOST, p_form_start); 
    curl_easy_setopt (mp->curl, CURLOPT_HEADER, 1);
    curl_easy_setopt (mp->curl, CURLOPT_WRITEFUNCTION, &pimpl::fill_response_buffer);
    curl_easy_setopt (mp->curl, CURLOPT_WRITEDATA, mp);

    // set external options
    curl_easy_setopt (mp->curl, CURLOPT_TIMEOUT, mp->timeout);
    if (mp->verbose) {
      curl_easy_setopt (mp->curl, CURLOPT_VERBOSE, 1);
    }

    // perform request
    CURLcode status;
    status = curl_easy_perform (mp->curl);

    if (status != CURLE_OK) {
      ofLogError() << "Agent request failed: " << curl_easy_strerror (status);
    }

    // cleanup
    curl_formfree (p_form_start);

    return mp->parse_response_buffer();
  }

  Response 
  Agent::POST (const std::string &url, 
	       const ofBuffer & data, 
	       const std::string &content_type)
  {
    return ofxREST::Response();
  }
  
  Response 
  Agent::PUT (const std::string &url, 
	      const ofBuffer &data, 
	      const std::string &content_type)
  {
    return ofxREST::Response();
  }

  Response
  Agent::DELETE (const std::string &url)
  {
    return ofxREST::Response();
  }

  void 
  Agent::set_timeout(int t) 
  {
    mp->timeout = t;
  }

  void 
  Agent::set_verbose(bool v)
  {
    mp->verbose = v;
  }  
} // namespace ofxRest

//
// helper functions 
//
  
static std::string 
generate_request_url (ofxREST::FormRequest & form) 
{
  std::string url = form.get_action();
  const ofxREST::FormRequest::value_map &values = form.get_values();
  if (values.empty()) {
    return url;
  }

  ofxREST::FormRequest::value_map::const_iterator vit = values.begin();

  url += "?";
  while (std::distance (vit, values.end()) > 1) {
    url += (vit->first + "=" + vit->second);
    url += "&";
    vit++;
  }
  url += (vit->first + "=" + vit->second);

  return url;
}

static string 
get_url_filename (const std::string &url){
  return url.substr (url.rfind('/') + 1);
}
