#ifndef _OFX_REST
#define _OFX_REST

#include "ofxREST_Types.h"

namespace ofxREST {
 
  class Agent {
    
  public:
    
    Agent();
    ~Agent();

    // Fetches a remote resource
    Response GET (const std::string &url);
    
    // Saves a remote resource to a file
    Response GET (const std::string &url, const std::string &save_path);
    
    // Builds a query string from a FormRequest to fetch a remote resource
    Response GET (const FormRequest &form);

    // Post multi-field form data and files to a remote resource
    Response POST (const FormRequest &form);

    // Post the contents of an ofBuffer to a remote resource
    Response POST (const std::string &url, 
		   const ofBuffer & data, 
		   const std::string &content_type="");

    // Updates a remote resource with the contents of an ofBuffer
    Response PUT (const std::string &url, 
		  const ofBuffer & data, 
		  const std::string &content_type="");
    
    // Deletes a remote resource 
    Response DELETE (const std::string &url);
    
    void set_timeout(int t);  
    void set_verbose(bool v);
    
  private:
    struct pimpl;
    pimpl *mp;
  };

} // namespace OFX_REST

#endif
