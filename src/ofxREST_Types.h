#ifndef OFX_REST_TYPES_H
#define OFX_REST_TYPES_H

#include "ofUtils.h"

#include <map>

namespace ofxREST {

  enum RequestMethod {GET, POST, PUT, DELETE};

  class FormRequest {
    
  public:

    typedef std::map<std::string, std::string> value_map;

    FormRequest (string action, string name = "form"):
      m_action (action),
      m_name (name)
    {}

    ~FormRequest(){
      clear();
    }

    void add_field (string id, string value){
      m_values[id] = value;
    }

    void add_file (string field_name, string path){
      m_files[field_name] = ofToDataPath (path);
    }

    void clear(){
      m_values.clear();
      m_files.clear();
    }

    std::string get_action() const {
      return m_action;
    }

    std::string get_value (std::string field_name) const {
      value_map::const_iterator it =
	m_values.find(field_name);
      if (it == m_values.end()) { return ""; }
      return it->second;
    }

    const value_map &
    get_values() const {
      return m_values;
    }

    const value_map &
    get_files() const {
      return m_files;
    }

  private:

    string m_action;
    string m_name;
    value_map m_values;
    value_map m_files;
  };


  struct Response{

    Response():
      status(200),
      reason("OK"),
      body(),
      content_type("text/http"),
      timestamp(0),
      url("http://localhost/"),
      location("")
    {}
    
    int status; 	   // return code for the response ie: 200 = OK
    std::string reason;    // text explaining the status
    ofBuffer body;         // the actual response
    string content_type;   // the mime type of the response
    long timestamp;	   // time of the response
    string url;
    string location;
  };
 
} // namespace ofxREST

#endif // OFX_REST_TYPES_H
